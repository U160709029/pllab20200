#include <stdio.h>
#include <stdlib.h>

int main()
{
   #include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct Skater{
char name[30],surname[30];
double referee_scores[6];
double avg;
}Skater;

double calculateScore(Skater *p);
double findMax(double arr[]);
double findMin(double arr[]);
void loadSkaters(Skater skaters[]);

int main()
{
Skater skaters[4];
loadSkaters(skaters);
}

double calculateScore(Skater *skater){
double max,min,sum=0;
double result;
for(int i=0;i<6;++i){
    sum+=(*skater).referee_scores[i];
}
max = findMax((*skater).referee_scores);
min = findMin((*skater).referee_scores);
sum = sum - (max+min);
return sum/8;
}
double findMax(double arr[]){
double max = arr[0];
int i;
for(i=0;i<6;i++){
    if(max<arr[i]) max=arr[i];
}
return max;

}
double findMin(double arr[6]){
double min = arr[0];
int i;
for(i=0;i<6;i++){
    if(min>arr[i]) min=arr[i];
}
return min;

}

void loadSkaters(Skater skaters[]){
FILE *outp;
outp= fopen("skaters.txt","r");

for(int i =0;i<4;++i){
    fscanf(outp,"%s %s %lf %lf %lf %lf %lf %lf",skaters[i].name,skaters[i].surname,&skaters[i].referee_scores[0],&skaters[i].referee_scores[1],
           &skaters[i].referee_scores[2],&skaters[i].referee_scores[3],&skaters[i].referee_scores[4],&skaters[i].referee_scores[5]);

           skaters[i].avg = calculateScore(&skaters[i]);

printf("**Skater %d: %s %s**\nAverage point of skater %d is: %.2f\n\n\n\n",i,skaters[i].name,skaters[i].surname,i,skaters[i].avg);
}

fclose(outp);
int max=0;
int temp=0;
for(int j=0;j<4;++j){
 if(max<skaters[j].avg){
    max=skaters[j].avg;
    temp=j;
 }

}
printf("Winner of the Skating Championship is:\n%s %s with %.2f points in total.\n\n\n",skaters[temp].name,skaters[temp].surname,skaters[temp].avg);
}

}
